(* ::Package:: *)

(* General conversion factors: *)

BeginPackage["Constants`"];

MeV$fm       = 197.326960;
MeV$cm       = MeV$fm * 10^(-13);
MeV$m        = MeV$fm * 10^(-15);
MeV$km       = MeV$cm*10^(-5);
MeV$sec      = 6.58212*10^(-22);
MeV$per$gram = 5.60959*10^26;
gram$per$MeV = 1/MeV$per$gram;
MeV$per$kg   = 1000*MeV$per$gram;
kg$per$MeV   = gram$per$MeV/1000;
dyne$per$MeV2= gram$per$MeV*MeV$cm/MeV$sec^2;
MeV2$per$dyne= 1/dyne$per$MeV2;


K$per$eV     = 11605.;
K$per$MeV    = K$per$eV* 10^6;
eV$per$K     = 1/K$per$eV;
MeV$per$K    = 1/K$per$MeV;

e$C          = 1.60217646 * 10^(-19);  
J$per$eV     = e$C;
J$per$MeV    = J$per$eV*10^6;
eV$per$J     = 1/J$per$eV;
MeV$per$J    = 1/J$per$MeV;

erg$per$MeV  = 10^7*J$per$MeV;
MeV$per$erg  = 10^(-7)*MeV$per$J;


m$per$sec    = 2.99792458 * 10^8;
cm$per$sec   = 100*m$per$sec;
sec$per$year = 31556925.9747  (* standard SI year of 365.242 days *)

Mneutron$MeV = 939.565378;
Mneutron$gram = Mneutron$MeV * gram$per$MeV;
Mproton$MeV = 938.272013;
Mproton$gram = Mproton$MeV * gram$per$MeV;

Melectron$MeV = 0.51100;
Melectron$gram = Melectron$MeV / MeV$per$gram;

Mpi0$MeV = 134.9768;
Mpi0$gram = Mpi0$MeV / MeV$per$gram;
(* Weak interaction *)

G$Fermi = 1.16637*10^(-11);  (* MeV^-2 *)
Cabbibo$angle=13.02*2\[Pi]/360;
(* electromagnetism: *)

epsilon$SI = 8.85418782*10^(-12);
hbar$SI = 1.05457148*10^(-34);
c$SI = 2.99792458*10^8; 

MeV2$per$Tesla = Sqrt[epsilon$SI * hbar$SI^3 * c$SI^5]/J$per$MeV^2;
MeV2$per$Gauss = MeV2$per$Tesla/10^4;
Tesla$per$MeV2 = 1/MeV2$per$Tesla;
Gauss$per$MeV2 = 1/MeV2$per$Gauss;

(* 
Tesla is J s m^-2 C^-1     (from F=B*I*l for example)
epsilon$SI is C^2 J^-1 m^-1  ( from V = q^2/(4 pi eps0 r) for example )
hbar$SI is J s
c$SI is m s^-1
so Tesla * Sqrt[epsilon$SI * hbar$SI^3 * c$SI^5] is J^2
then convert to MeV
*)

(*
Phi0=2*Pi/(2*e);  flux quantum for electron or proton pairs 
Phi0$SI = Phi0* MeV$m^2/MeV2$per$Tesla;   (T m^2)
*)

(*
alpha = 1/137.035999084;
e = Sqrt[4*Pi*alpha]; 
Phi0=2*Pi/(2*e);   
*)

(* Nuclear physics *)
NuclearDensity$nucleons$per$fm3 = 0.16;  (* conventional value *)
NuclearDensity$nucleons$MeV3 = NuclearDensity$nucleons$per$fm3*MeV$fm^3;
NuclearDensity$quarks$MeV3 = 3*NuclearDensity$nucleons$MeV3;

NuclearEnergyDensity$MeV4 = (Mneutron$MeV-8)*NuclearDensity$nucleons$MeV3;
(* assumes 8 MeV per nucleon binding energy *)
NuclearEnergyDensity$g$per$cm3=NuclearEnergyDensity$MeV4/MeV$per$gram/(MeV$cm)^3;



(* Astro Consts *)
MSolar$gram = 1.98847*10^33;
MSolar$MeV  = MSolar$gram*MeV$per$gram;
BSolar = MSolar$MeV/Mneutron$MeV;

NewtonG$SI=6.67428*10^(-11); (* m^5 s^-4 J^-1 *)
NewtonG = NewtonG$SI/(hbar$SI*c$SI^5) * J$per$MeV^2;  (* MeV^(-2) *)


EndPackage[];

Protect["Constants`*"];


