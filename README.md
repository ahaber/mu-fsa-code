# mU FSA code

This code computes modified Urca rates in the Fermi Surface approximation using the crude $1/\mu_e$ approximation for the internal propagator. All the code can be found in the example notebook. The repo also contains EOS tables for the QMC-RMF3 EOS and the TMA EOS at $T=0$, and $T=[0.5,1,3,5]$ MeV in cold chemical equilibrium $\mu_n=\mu_p+\mu_e$. The code is set up to also compute the rates out of equilibrium, but the EOS tables are not included atm.

